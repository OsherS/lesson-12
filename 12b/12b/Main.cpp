#include "MessagesSender.h"

int main()
{
	MessagesSender foo;
	thread t1 (&MessagesSender::minuteReader, &foo);
	thread t2(&MessagesSender::msgWriter, &foo);


	t1.detach();
	t2.detach();

	foo.MessagesSender::menu();
	
	return 0;
}