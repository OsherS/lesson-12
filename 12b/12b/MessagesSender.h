#pragma once
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <algorithm>
#include <queue>
#include<fstream>
#include <string>
#include<windows.h>



#define PATH "data.txt"


using namespace std;

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void menu();
	void signIn();
	void signOut();
	void showAllUsers();
	void minuteReader();
	void msgWriter();

private:
	vector<string> users;
	queue<string> msgs;
	mutex userLock;
	mutex msgLock;
};