#include "MessagesSender.h"

//nothing to intallize
MessagesSender::MessagesSender()
{
}

//nothing to delete
MessagesSender::~MessagesSender()
{
}

 //shows menu
void MessagesSender::menu()
{
	int c = 0;
	while (c != 4)
	{
		cout << "Welcome!\n 1. Signin\n 2. Signout\n 3. Connected Users\n 4. exit\n";
		cin >> c;

		if (c == 1)
		{
			signIn();
		}
		if (c == 2)
		{
			signOut();
		}
		if (c == 3)
		{
			showAllUsers();
		}
	}
}

//adding a name to name vector
void MessagesSender::signIn()
{
	string name = "";

	unique_lock<mutex> lock(userLock);
	cout << "Enter name: ";
	lock.unlock();

	cin >> name;
	
	if (!(find(users.begin(), users.end(), name) != users.end()))
	{
		unique_lock<mutex> lock(userLock);
		users.push_back(name);
		cout << "User added\n";
		lock.unlock();
	}
	else
	{
		unique_lock<mutex> lock(userLock);
		cout << "User exists";
		lock.unlock();

	}

}

//deleteing a name from name vector
void MessagesSender::signOut()
{
	string name = "";
	unique_lock<mutex> lock(userLock);
	cout << "Enter name : ";
	cin >> name;
	users.erase(std::remove(users.begin(), users.end(), name), users.end());
	cout << "If the user existed, now it's not.";
	lock.unlock();

}

 //printing all existing user names
void MessagesSender::showAllUsers()
{
	unique_lock<mutex> lock(userLock);
	for (int i = 0; i < users.size(); i++)
	{
		cout << users[i] << " ";
	}
	lock.unlock();
}

 //reading data file every minute and putting it into msg queue
void MessagesSender::minuteReader()
{
	while (true)
	{
		ifstream myfile(PATH);
		string line;

		unique_lock<mutex> lock(msgLock);
		while (getline(myfile, line))
		{
			msgs.push(line);
		}
		lock.unlock();

		ofstream ofs;
		ofs.open("data.txt", ofstream::out | ofstream::trunc);
		ofs.close();
		Sleep(1000000 * 60);
	}
}

//writing msgs to output file
void MessagesSender::msgWriter()
{
	ofstream myfile;
	myfile.open("output.txt");
	while (!msgs.empty())
	{
		myfile << "to: ";
		showAllUsers();
		myfile << "\n" << msgs.front() << " ";
		unique_lock<mutex> lock(msgLock);
		msgs.pop();
		lock.unlock();

	}

}





