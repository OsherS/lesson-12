#include <iostream>
#include <thread>
#include <vector>
#include <ctime>
#include <chrono>
#include <fstream>
#include <windows.h> 
#include <mutex>



using namespace std;
void I_Love_Threads();
void call_I_Love_Threads();
void getPrimes(int begin, int end, std::vector<int>& primes);
bool isPrime(int num);
void printVector(std::vector<int> primes);
vector<int> callGetPrimes(int begin, int end);
void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
void printColor();
void callColor();

int main()
{


    //calling function 3 times with diff arguments
    callWritePrimesMultipleThreads(1, 1000, "txt.txt", 2); 
    callWritePrimesMultipleThreads(1, 100000, "txt.txt", 2);
    callWritePrimesMultipleThreads(1, 1000000, "txt.txt", 2);
    
	return 0;
}

//prints I love threads
void I_Love_Threads()
{
	cout << "I Love Threads";
}

//calls I_Love_Threads as a thread
void call_I_Love_Threads()
{
	thread t1 (I_Love_Threads);
	t1.join();
}

//putting all i prime values in the given vector
//i = all numbers from int begin to in end
void getPrimes(int begin, int end, std::vector<int>& primes)
{
    for (int i = begin; i <= end; i++)
    {
        if (isPrime(i)) //prime check
        {
            primes.push_back(i);
        }
    }
}

//checks if a number is prime
bool isPrime(int num) 
{
    bool flag = true;
    for (int i = 2; i <= num / 2; i++) 
    {
        if (num % i == 0)
        {
            flag = false;   
            break;
        }
    }
    return flag;
}

//int vector printing function line by line
void printVector(std::vector<int> primes)
{
    for (int i = 0; i < primes.size(); i++)
    {
        cout << primes[i] << "\n"; //printing int and adding a new line
    }
}

//calls getPrimes as a thread 
//counts getPrimes run time
//returns vector with prime numbers between begin to end
vector<int> callGetPrimes(int begin, int end)
{
    vector<int> vect;
    double elapsed_secs;
    chrono::microseconds dur;
    chrono::steady_clock::time_point start, finish; 
    start = chrono::high_resolution_clock::now(); // get current time before running the thread
    thread t1(getPrimes,begin,end, ref(vect));
    t1.join();
    finish = chrono::high_resolution_clock::now(); // get currnt time after running the thread
    dur = chrono::duration_cast<chrono::microseconds>(finish - start); // calculate run time
    cout << "\nFunction run time in microseconds is: " << dur.count();
    return vect;
}

//wrties all the prime numbers in the range into a given file
void writePrimesToFile(int begin, int end, ofstream& file)
{
    mutex locker;
    for (int i = begin; i <= end; i++)
    {
        if (isPrime(i)) //prime check
        {
            unique_lock<mutex> l(locker);
            file << i << "\n";
            l.unlock();
        }
    }
}

//creates N threads
//in N diff ranges that cover all the numbers from begin to end
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
    ofstream myfile;
    vector<int> vect;
    double elapsed_secs;
    chrono::microseconds dur;
    chrono::steady_clock::time_point start, finish;
    myfile.open(filePath);
    end /= N;
    start = chrono::high_resolution_clock::now(); // get current time before running the thread creating loop
    for (int i = 1; i <= N; i++)
    {
        end *= i;
        thread t1(writePrimesToFile, begin, end, ref(myfile));
        t1.join();
        begin = end;   
    }
    finish = chrono::high_resolution_clock::now(); // get currnt time after running the loop
    dur = chrono::duration_cast<chrono::microseconds>(finish - start); // calculate run time
    cout << "\nall threads run time in microseconds is: " << dur.count();
}


void printColor()
{
    for (int i = 0; i < 10; i++)
    {
        cout << "Color";
    }
}

void callColor()
{
    for (int color = 1; color <= 15; color++)
    {
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
         thread t1(printColor);
         t1.join();
    }
}

